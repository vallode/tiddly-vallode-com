# tiddly-vallode-com

Space for my personal mind garden, using tiddlywiki.

Run with `./run.sh`
Build static website with `./build.sh`

## Plugins used

Stroll: https://giffmex.org/stroll/stroll.html